resource "aws_ecs_service" "mongo" {
  name            = "mongodb"
  cluster         = "gitlab"
  task_definition = "mongodb"
  desired_count   = 1
  deployment_maximum_percent = 100
  deployment_minimum_healthy_percent = 50
}

resource "aws_ecs_task_definition" "mongodb" {
  family                = "mongodb"
  container_definitions = "${file("taskFile.json")}"
}

# create task definition

